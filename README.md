# django-cli

Add packages, edit and remove them with ease!

## Install

### Latest published version

```bash
python3 -m pip install django-cli
```

### Latest dev version (**warning! unstable!**)

```bash
python3 -m pip install git+https://gitlab.com/sodimel/django-cli
```

## Use

```bash
python3 -m django-cli <action> [arg(s)]
```

## Setup a Django project

### With django-cli

```
python3 -m django-cli newproject [name]
```

### Without django-cli

```
mkdir mydumbproject && cd mydumbproject
python3 -m venv .venv
. .venv/bin/activate
python3 -m pip install django
python3 -m django startproject mydumbproject
```