from click import echo


def secho(txt, sp, err=False):
    """
    Separator + click's echo function = secho
    """
    echo(" " * sp, nl=False)
    echo(txt, err=err)
