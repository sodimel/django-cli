import os
import venv
from subprocess import check_call

from click import style

from .print import secho as echo


def create_new_project(name, sp):
    """
    Create folder, and then create venv, and then install django.
    """
    echo(f"Creating folder {style(name, fg='green')}...", sp)
    try:
        os.mkdir(name)
    except FileExistsError:
        pass
    echo("Done!", sp)

    venv_path = os.path.join(name, ".venv")

    echo(f"Creating venv in {style(venv_path, fg='green')}...", sp)
    new_venv = venv.EnvBuilder(with_pip=True)
    new_venv.create(env_dir=venv_path)
    echo("Done!", sp)

    echo("Installing django in venv...", sp)
    check_call([os.path.join(venv_path, "bin", "pip"), "install", "django"])
    echo("Done!", sp)

    echo("Creating website...", sp)
    found_folder = False
    for i in os.listdir(os.path.join(venv_path, "lib")):
        if "python" in i:
            try:
                check_call(
                    [
                        os.path.join(".venv", "bin", "python3"),
                        "-m",
                        "django",
                        "startproject",
                        name,
                    ],
                    cwd=name,
                )
            except:  # noqa
                echo(style("Can't create project.", fg="red"), sp, err=True)
                pass
            found_folder = True
            break
    if not found_folder:
        echo(
            style(
                f"Folder {os.path.join(venv_path, 'lib', 'pythonXX', 'site-packages', 'django')} was not found.",
                fg="red",
            ),
            sp,
            err=True,
        )
