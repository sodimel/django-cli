import click

sp = 0


@click.group()
def commands():
    pass


@commands.command()
def install():
    """
    Install module. Check in pyproject.toml or in list on django-cli.l3m.in for config.
    """
    click.echo("todo")


@commands.command()
def config():
    """
    Config django-cli & project. Accept toml config files.
    """
    click.echo("todo")


@commands.command()
def convert():
    """
    Convert an old django settings.py file to a brand new django-cli-compliant settings.py file.
    """
    click.echo("todo")


@commands.command()
@click.argument("name", default="mydumbproject")
def newproject(name):
    """
    Create a new venv and install a django project in it's own folder.
    """
    click.echo(f"Creating django project in {click.style(name, fg='green')} folder...")

    from .utils import newproject

    newproject.create_new_project(name, sp + 2)


if __name__ == "__main__":
    commands()
